#!/usr/bin/env node
'use strict';

const chalk = require('chalk');
const commander = require('commander');
const path = require('path');
const semver = require('semver');
const fs = require('fs-extra');

let projectName;
const packageJson = require('../package.json');

const program = new commander.Command(packageJson.name)
    .version(packageJson.version)
    .arguments('<project-directory>')
    .usage(`${chalk.green('<project-directory>')} [options]`)
    .action(name => {
        projectName = name;
    })
    .option('--verbose', 'print additional logs')
    .allowUnknownOption()
    .on('--help', () => {
        console.log(`    Only ${chalk.green('<project-directory>')} is required.`);
        console.log();
    })
    .parse(process.argv);

if (typeof projectName === 'undefined') {
    console.error('Please specify the project directory:');
    console.log(
        `  ${chalk.cyan(program.name())} ${chalk.green('<project-directory>')}`
    );
    console.log();
    console.log('For example:');
    console.log(`  ${chalk.cyan(program.name())} ${chalk.green('my-react-app')}`);
    console.log();
    console.log(
        `Run ${chalk.cyan(`${program.name()} --help`)} to see all options.`
    );
    process.exit(1);
}

function printValidationResults(results) {
    if (typeof results !== 'undefined') {
        results.forEach(error => {
            console.error(chalk.red(`  *  ${error}`));
        });
    }
}

const hiddenProgram = new commander.Command()
    .option(
    '--internal-testing-template <path-to-template>',
    '(internal usage only, DO NOT RELY ON THIS) ' +
    'use a non-standard application template'
    )
    .parse(process.argv);

createApp(
    projectName,
    program.verbose,
    program.scriptsVersion,
    hiddenProgram.internalTestingTemplate
);

function isSafeToCreateProjectIn(root, name) {
    const validFiles = [
        '.DS_Store',
        'Thumbs.db',
        '.git',
        '.gitignore',
        '.idea',
        'README.md',
        'LICENSE',
        'web.iml',
        '.hg',
        '.hgignore',
        '.hgcheck',
    ];
    console.log();

    const conflicts = fs
        .readdirSync(root)
        .filter(file => !validFiles.includes(file));
    if (conflicts.length < 1) {
        return true;
    }

    console.log(
        `The directory ${chalk.green(name)} contains files that could conflict:`
    );
    console.log();
    for (const file of conflicts) {
        console.log(`  ${file}`);
    }
    console.log();
    console.log(
        'Either try using a new directory name, or remove the files listed above.'
    );

    return false;
}

function createApp(name, verbose, version, template) {
    const root = path.resolve(name);
    const appName = path.basename(root);

    fs.ensureDirSync(name);
    if (!isSafeToCreateProjectIn(root, name)) {
        process.exit(1);
    }

    console.log(`Creating a new app in ${chalk.green(root)}.`);
    console.log();

    const packageJson = require(path.join(__dirname, '../template/package.json'));
    packageJson.name = appName;
    packageJson.version = version;
    fs.copySync(path.join(__dirname, '../template'), root, {
        dereference: true,
    });
    fs.writeFileSync(
        path.join(root, 'package.json'),
        JSON.stringify(packageJson, null, 2)
    );
    console.log(chalk.green(`Success! Created ${chalk.red(appName)} at ${root}`));
    const originalDirectory = process.cwd();
    process.chdir(root);

    if (!semver.satisfies(process.version, '>=6.0.0')) {
        console.log(
            chalk.yellow(
                `You are using Node ${process.version} so the project will be bootstrapped with an old unsupported version of tools.\n\n` +
                `Please update to Node 6 or higher for a better, fully supported experience.\n`
            )
        );
    }
}