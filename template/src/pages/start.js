import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { injectIntl, FormattedMessage } from 'react-intl';
import { Button, Modal, message } from 'antd';
import { Http } from 'brc-util';
import Header from 'components/header';
import BasicInfo from 'components/basic-info';
import ListInfo from 'components/list-info/editable';
import SuccessTip from 'components/success-tip';
import logo from '../images/logo.png';
import BasicInfoStore from 'models/basic-info';
import ListStore from 'models/list';
import Store from 'models';
import api from 'api';
import { homeMessages, btnMessages, confirmMessages } from '../locales';
import '../app.css';

const confirm = Modal.confirm;

@observer
class App extends Component {
  componentDidMount() {
    // this.initProcessData();
    Object.assign(BasicInfoStore, {
      Applicant: '阿炎',
      Department: '研发部',
      ApplicationDate: new Date(),
      Folio: 'TR201709150001',
    });
  }

  /**
   * 初始化表单数据
   */
  initProcessData() {
    // 初始化数据
    Http.get(`${api}/BRC.Reimbursement.API/process/initdata`)
      .then(json => {
        Object.assign(BasicInfoStore, json.master);
        ListStore.load(json.reimbursementinfo);
      })
  }
  
  /**
   * 弹出提交确认框
   */
  showConfirm() {
    if (ListStore.reportPostDataList.length === 0) {
      message.config({
        top: 200,
      });
      message.warning('Please fill in the reimbursement detail');
      return false;
    }
    const isValid = ListStore.checkValid();
    if (!isValid) return false;
    confirm({
      title: window.formatMessage(confirmMessages.submitTitle),
      content: window.formatMessage(confirmMessages.submitContent),
      onOk: () => {
        this.startProcess();
      },
      onCancel() {
      },
    });
  }
  
  /**
   * 发起流程
   */
  startProcess() {
    // 获取表单数据
    const values = {
      master: BasicInfoStore,
      reimbursementinfo: ListStore.reportPostDataList,
    }
    console.log(values);
    // 调用接口
    Http.post('http://localhost/ConsulTest/api/values?id=5',values)
      .then(json => {
        Store.submited = true;
      })
  }
  
  /**
   * 弹出保存确认框
   */
  showDraftConfirm() {
    // 弹出确认框
    confirm({
      title: window.formatMessage(confirmMessages.saveTitle),
      content: window.formatMessage(confirmMessages.saveContent),
      onOk: () => {
        this.saveDraft();
      }
    });
  }

  /**
   * 保存草稿
   */
  saveDraft() {
    // 获取表单数据
    const values = {
      master: BasicInfoStore,
      reimbursementinfo: ListStore.reportPostDataList,
    }
    console.log(values);
    // Todo 调用接口
  }
  
  render() {
    const { intl } = this.props;
    window.formatMessage = intl.formatMessage;
    if (Store.submited) return <SuccessTip />;
    return (
      <form className="ant-advanced-search-form">
        <Header
          title={intl.formatMessage(homeMessages.title)}
          logo={logo}
        />
        <div style={{ textAlign: 'right', width: '100%' }}>
          <Button
            type="primary"
            onClick={() => { this.showConfirm() }}
            className="bedrock-btn"
          >
            <FormattedMessage {...btnMessages.submit}  />
          </Button>
          <Button
            type="primary"
            onClick={() => { this.showDraftConfirm() }}
            className="bedrock-btn"
          >
          <FormattedMessage {...btnMessages.save}  />
          </Button>
        </div>
        <BasicInfo data={BasicInfoStore} />
        <ListInfo data={ListStore} />
      </form>
    );
  }
}

export default injectIntl(App);
