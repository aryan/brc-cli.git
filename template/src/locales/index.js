import { defineMessages } from 'react-intl';

const lanMessages = defineMessages({
  zh: {
    id: 'lan.zh',
    defaultMessage: 'Chinese',
  },
  en: {
    id: 'lan.en',
    defaultMessage: 'English'
  }
});

const btnMessages = defineMessages({
  submit: {
    id: 'btn.submit',
    defaultMessage: '提交',
  },
  save: {
    id: 'btn.save',
    defaultMessage: '保存'
  }
});

const confirmMessages = defineMessages({
  submitTitle: {
    id: 'confirm.submit.title',
    defaultMessage: '确认提交？',
  },
  submitContent: {
    id: 'confirm.submit.content',
    defaultMessage: '点击确认按钮提交申请！',
  },
  saveTitle: {
    id: 'confirm.save.title',
    defaultMessage: '确认保存？',
  },
  saveContent: {
    id: 'confirm.save.content',
    defaultMessage: '点击确认按钮保存申请！',
  },
});

const homeMessages = defineMessages({
  title: {
    id: 'app.header.title',
    defaultMessage: '磐荣表单框架模板',
  },
});

const basicMessages = defineMessages({
  title: {
    id: 'basic.title',
    defaultMessage: '基础信息',
  },
  applicant: {
    id: 'basic.applicant',
    defaultMessage: '申请人',
  },
  department: {
    id: 'basic.department',
    defaultMessage: '部门',
  },
  date: {
    id: 'basic.applicationdate',
    defaultMessage: '申请日期',
  },
  folio: {
    id: 'basic.folio',
    defaultMessage: '单号',
  },
});

const detailMessages = defineMessages({
  title: {
    id: 'detail.title',
    defaultMessage: '明细信息',
  },
  sn: {
    id: 'detail.sn',
    defaultMessage: '序号',
  },
  eventdate: {
    id: 'detail.eventdate',
    defaultMessage: '费用发生时间',
  },
  costcenter: {
    id: 'detail.costcenter',
    defaultMessage: '成本中心',
  },
  expensecategory: {
    id: 'detail.expensecategory',
    defaultMessage: '费用科目',
  },
  amount: {
    id: 'detail.amount',
    defaultMessage: '金额',
  },
  remark: {
    id: 'detail.remark',
    defaultMessage: '备注',
  },
  addBtn: {
    id: 'detail.addBtn',
    defaultMessage: '新增',
  },
});

export {
  homeMessages,
  btnMessages,
  confirmMessages,
  lanMessages,
  basicMessages,
  detailMessages,
}