import React, { Component } from 'react';
import { observer } from 'mobx-react';
import moment from 'moment';

@observer
class TableCell extends Component {
  render() {
    const { model, id } = this.props;
    console.log(JSON.stringify(model));
    return (
      <tr>
        <td><span>{id + 1}</span></td>
        <td>
          <span>{model.OperatorAd}</span>
        </td>
        <td>
          <span>{model.Operator}</span>
        </td>
        <td>
          <span>{model.ActivityName}</span>
        </td>
        <td>
          <span>{model.Action}</span>
        </td>
        <td>
          <span>{model.Content}</span>
        </td>
        <td>
          <span>{moment(model.CreateDate).format('YYYY-MM-DD hh:mm:ss')}</span>
        </td>
      </tr>
    );
  }
}

export default TableCell;
