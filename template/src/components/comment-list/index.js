import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { Row, Col, Icon } from 'antd';
import { Table } from 'lib/ui';
import TableCell from './cell';

@observer
class ReimbursementList extends Component {
  constructor() {
    super();
    this.renderHeader = this.renderHeader.bind(this);
    this.renderRow = this.renderRow.bind(this);
    this.renderPrefixHeader = this.renderPrefixHeader.bind(this);
    this.renderPrefixRow = this.renderPrefixRow.bind(this);
    this.renderFooter = this.renderFooter.bind(this);
  }

  renderHeader(data) {
    return (
      <tr>
        <th style={{ width: '3%' }}>SN</th>
        <th>Operator AD</th>
        <th>Operator</th>
        <th>ActivityName</th>
        <th>Action</th>
        <th>Content</th>
        <th>CreateDate</th>
      </tr>
    );
  }
  renderPrefixHeader() {
    return (
      <tr>
        <th style={{ width: '5%' }}>SN</th>
      </tr>
    );
  }
  renderRow() {
    const { data: { list } } = this.props;
    const rows = list.map((item, index) => (
      <TableCell
        model={item}
        id={index}
        key={index}
      />
      ));
    return rows;
  }

  renderPrefixRow() {
    const prefixRows = this.props.data.list.map((field, index) => (
      <tr key={index}>
        <td>{index + 1}</td>
      </tr>
      ));
    return prefixRows;
  }

  renderFooter() {
    return <div></div>;
  }

  render() {
    return (
      <div>
        <div style={{ marginBottom: 10, marginTop: 10 }}>
          <Icon type="book" className="ant-icon" />
          <span className="ant-title">Comments Log</span>
        </div>
        <Row gutter={80}>
          <Col span={24}>
            <Table
              renderFooter={this.renderFooter}
              renderHeader={this.renderHeader}
              renderRow={this.renderRow}
              renderPrefixHeader={this.renderPrefixHeader}
              renderPrefixRow={this.renderPrefixRow}
              headshow="none"
            />
          </Col>
        </Row>
      </div>
    );
  }
}

export default ReimbursementList;