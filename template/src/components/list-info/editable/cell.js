import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { Icon } from 'antd';
import moment from 'moment';
import { Format } from 'brc-util';
import { Input, Select, DatePicker, InputFormat } from 'lib/ui';
import api from '../../../api';

const costcenterData = [
  { value: 'fin', text: '财务部' },
  { value: 'hr', text: '人事部' },
  { value: 'tec', text: '技术部' },
];

@observer
class TableCell extends Component {

  getExpenseCategoryData(costcentercode) {
    let data;
    switch (costcentercode) {
      case 'fin':
        data = [
          { value: 'guding', text: '固定资产' },
          { value: 'dizhi', text: '低值易耗及维修' }
        ]
        break;
      case 'hr':
        data = [
          { value: 'emp', text: '员工培训' },
          { value: 'tb', text: '团队建设' },
          { value: 'sybx', text: '商业保险' },
        ]
        break;
      case 'tec':
        data = [
          { value: 'tr', text: '差旅费用' },
          { value: 'loc', text: '本地费用' },
        ]
        break;
      default:
        data = [
          { value: 'guding', text: '固定资产' },
          { value: 'dizhi', text: '低值易耗及维修' }
        ]
        break;
    }
    return data;
  }

  render() {
    const { model, id, isFinance } = this.props;
    console.log(JSON.stringify(model));
    return (
      <tr>
        <td><span>{id + 1}</span></td>
        <td>
          <DatePicker
            style={{ width: '100%' }}
            value={model.EventDate ? moment(model.EventDate) : null}
            onChange={(data, dataStr) => {
              model.EventDate = dataStr;
            }}
            validator={model.errors.EventDate}
          />
        </td>
        <td>
          <Select
            placeholder="请选择..."
            remote
            onSelect={(value, option) => {
              model.onCostCenterCodeChange(value, option.props.children);
            }}
            style={{ width: '100%' }}
            defaultText={model.CostCenterName}
            defaultValue={model.CostCenterCode}
            value={model.CostCenterCode}
            data={costcenterData}
            dataUrl={`${api}/BRC.Reimbursement.API/costcenter/data`}
          />
        </td>
        <td>
          <Select
            remote
            placeholder="请选择..."
            style={{ width: '100%' }}
            defaultText={model.ExpenseCategoryName}
            defaultValue={model.ExpenseCategoryCode}
            value={model.ExpenseCategoryCode}
            onSelect={(value, option) => {
              model.onExpenseCategoryCodeChange(value, option.props.children);
            }}
            data={this.getExpenseCategoryData(model.CostCenterCode)}
            dataUrl={`${api}/BRC.Reimbursement.API/expensecategory/data?code=${model.CostCenterCode}`}
          />
        </td>
        <td>
          <InputFormat
            placeholder="0.00"
            value={model.Amount}
            disabled={isFinance}
            onChange={e => {
              model.Amount = e.target.value;
            }}
            validator={model.errors.Amount}
            style={{ width: '100%' }}
            formatter={Format.money}
          />
        </td>
        <td>
          <Input
            placeholder=""
            value={model.Remark}
            onChange={e => {
              model.Remark = e.target.value;
            }}
            style={{ width: '100%' }}
          />
        </td>
        <td>
          <Icon
            style={{ fontSize: '16px', color: '#108ee9', cursor: 'pointer' }}
            type="edit"
            onClick={() => this.props.editRow(this.props.id)}
          />
          <Icon
            style={{ fontSize: '16px', color: 'red', cursor: 'pointer', marginLeft: 10 }}
            type="delete"
            onClick={() => this.props.deleteRow(this.props.id)}
          />
        </td>
      </tr>
    );
  }
}

export default TableCell;
