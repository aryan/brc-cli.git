import React, { Component } from 'react';
import { FormattedMessage } from 'react-intl';
import { Row, Col, Modal } from 'antd';
import { observer } from 'mobx-react';
import { Input, Select, DatePicker } from 'lib/ui';
import moment from 'moment';
import { detailMessages } from '../../../locales';

const costcenterData = [
  { value: 'fin', text: '财务部' },
  { value: 'hr', text: '人事部' },
  { value: 'tec', text: '技术部' },
];

@observer
class EditModal extends Component {

  getExpenseCategoryData(costcentercode) {
    let data;
    switch (costcentercode) {
      case 'fin':
        data = [
          { value: 'guding', text: '固定资产' },
          { value: 'dizhi', text: '低值易耗及维修' }
        ]
        break;
      case 'hr':
        data = [
          { value: 'emp', text: '员工培训' },
          { value: 'tb', text: '团队建设' },
          { value: 'sybx', text: '商业保险' },
        ]
        break;
      case 'tec':
        data = [
          { value: 'tr', text: '差旅费用' },
          { value: 'loc', text: '本地费用' },
        ]
        break;
      default:
        data = [
          { value: 'guding', text: '固定资产' },
          { value: 'dizhi', text: '低值易耗及维修' }
        ]
        break;
    }
    return data;
  }

  render() {
    const { data, ...other } = this.props;
    console.log(data);
    return (
      <Modal
        {...other}
      >
        <Row gutter={80}>
          <Col span={24}>
            <span className="form-input-label" >
              <FormattedMessage {...detailMessages.eventdate} />
            </span>
          </Col>
          <Col span={24}>
            <DatePicker
              style={{ width: '100%' }}
              value={data.EventDate ? moment(data.EventDate) : null}
              onChange={(date, dateStr) => {
                data.EventDate = dateStr;
              }}
              validator={data.errors.EventDate}
            />
          </Col>
          <Col span={24}>
            <span className="form-input-label" >
              <FormattedMessage {...detailMessages.costcenter} />
            </span>
          </Col>
          <Col span={24}>
            <Select
              placeholder="请选择..."
              remote
              onSelect={(value, option) => {
                data.onCostCenterCodeChange(value, option.props.children);
              }}
              style={{ width: '100%' }}
              defaultText={data.CostCenterName}
              defaultValue={data.CostCenterCode}
              value={data.CostCenterCode}
              data={costcenterData}
            />
          </Col>
          <Col span={24}>
            <span className="form-input-label" >
              <FormattedMessage {...detailMessages.expensecategory} />
            </span>
          </Col>
          <Col span={24}>
            <Select
              placeholder="请选择..."
              style={{ width: '100%' }}
              remote
              defaultText={data.ExpenseCategoryName}
              defaultValue={data.ExpenseCategoryCode}
              value={data.ExpenseCategoryCode}
              onSelect={(value, option) => {
                data.onExpenseCategoryCodeChange(value, option.props.children);
              }}
              data={this.getExpenseCategoryData(data.CostCenterCode)}
            />
          </Col>
          <Col span={24}>
            <span className="form-input-label" >
              <FormattedMessage {...detailMessages.amount} />
            </span>
          </Col>
          <Col span={24}>
            <Input
              placeholder="0.00"
              value={data.Amount}
              onChange={e => {
                data.Amount = e.target.value;
              }}
              validator={data.errors.Amount}
              style={{ width: '100%' }}
            />
          </Col>
          <Col span={24}>
            <span className="form-input-label" >
              <FormattedMessage {...detailMessages.remark} />
            </span>
          </Col>
          <Col span={24}>
            <Input
              className="form-input"
              value={data.Remark}
              onChange={e => data.Remark = e.target.value}
            />
          </Col>
        </Row>
      </Modal>
    );
  }
}

export default EditModal;