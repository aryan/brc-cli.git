import React, { Component } from 'react';
import { FormattedMessage } from 'react-intl';
import { observer } from 'mobx-react';
import { Row, Col, Button, Icon } from 'antd';
import { Table } from 'lib/ui';
import EditModal from './edit-modal';
import ListModel from 'models/list-model';
import TableCell from './cell';
import { detailMessages } from '../../../locales';

@observer
class LeaveInfoList extends Component {
  constructor() {
    super();
    this.addRowData = this.addRowData.bind(this);
    this.deleteRowData = this.deleteRowData.bind(this);
    this.renderHeader = this.renderHeader.bind(this);
    this.renderRow = this.renderRow.bind(this);
    this.renderFooter = this.renderFooter.bind(this);
    this.showModal = this.showModal.bind(this);
    this.hideModal = this.hideModal.bind(this);
    this.renderModal = this.renderModal.bind(this);
    this.saveCellData = this.saveCellData.bind(this);
  }

  showModal(rowIndex) {
    const { data } = this.props;
    this.rowIndex = rowIndex;
    data.modalVisibled = true;
  }

  hideModal() {
    const { data } = this.props;
    data.modalVisibled = false;
  }

  saveCellData() {
    const { data } = this.props;
    data.saveCellData(this.rowIndex, this.tempCellData);
  }

  addRowData() {
    const { data } = this.props;
    data.add();
  }

  deleteRowData(rowIndex) {
    console.log(rowIndex);
    const { data } = this.props;
    data.remove(rowIndex);
  }

  renderHeader(data) {
    return (
      <tr>
        <th style={{width: '7%'}}><FormattedMessage {...detailMessages.sn} /></th>
        <th style={{width: '17%'}}><FormattedMessage {...detailMessages.eventdate} /></th>
        <th style={{width: '17%'}}><FormattedMessage {...detailMessages.costcenter} /></th>
        <th style={{width: '17%'}}><FormattedMessage {...detailMessages.expensecategory} /></th>
        <th style={{width: '17%'}}><FormattedMessage {...detailMessages.amount} /></th>
        <th style={{width: '17%'}}><FormattedMessage {...detailMessages.remark} /></th>
        <th style={{width: '8%'}}></th>
      </tr>
    );
  }
  renderRow() {
    const { data: { list } } = this.props;
    const rows = list.map((item, index) => (
      <TableCell
        model={item}
        id={index}
        key={index}
        deleteRow={this.deleteRowData}
        editRow={this.showModal}
      />
      ));
    return rows;
  }

  renderFooter() {
    const { data: { reportTotalAmount } } = this.props;
    return (
      <div>
        <Button type="primary" icon="plus" onClick={this.addRowData} >{window.formatMessage(detailMessages.addBtn)}</Button>
        <div style={{float: 'right', fontSize: '15px', paddingRight: 10}}>{`Total Amount:${reportTotalAmount}`}</div>
      </div>
    );
  }

  renderModal() {
    const { data: { list, modalVisibled } } = this.props;
    if(modalVisibled) {
      this.tempCellData = new ListModel();
      console.log(this.tempCellData);
      //Object.assign(this.tempCellData, list[this.rowIndex]);
      this.tempCellData.EventDate = list[this.rowIndex].EventDate;
      this.tempCellData.CostCenterCode = list[this.rowIndex].CostCenterCode;
      this.tempCellData.ExpenseCategoryCode = list[this.rowIndex].ExpenseCategoryCode;
      this.tempCellData.Amount = list[this.rowIndex].Amount;
      this.tempCellData.Remark = list[this.rowIndex].Remark;
      return (
        <EditModal
          visible
          title={`第${this.rowIndex + 1}行数据`}
          onOk={this.saveCellData}
          onCancel={this.hideModal}
          data={this.tempCellData}
        />
      );
    }
    return null;
  }

  render() {
    return (
      <div>
        <div style={{ marginBottom: 10 }}>
          <Icon type="book" className="ant-icon" />
          <span className="ant-title"><FormattedMessage {...detailMessages.title} /></span>
        </div>
        <Row gutter={80}>
          <Col span={24}>
            <Table
              renderFooter={this.renderFooter}
              renderHeader={this.renderHeader}
              renderRow={this.renderRow}
            />
          </Col>
          {this.renderModal()}
        </Row>
      </div>
    );
  }
}

export default LeaveInfoList;