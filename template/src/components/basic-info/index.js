/**
 * Created by Aryan on 2016/7/19.
 */
import React, { Component} from 'react';
import { FormattedMessage } from 'react-intl';
import { observer } from 'mobx-react';
import { Row, Col, Icon } from 'antd';
import { Input, DatePicker } from 'lib/ui';
import { basicMessages } from '../../locales';
import moment from 'moment';

@observer
class BasicInfo extends Component {
  render() {
    const { data } = this.props;
    return (
      <div>
        <div>
          <Icon type="user" className="ant-icon" />
          <span className="ant-title">
            <FormattedMessage {...basicMessages.title} />
          </span>
        </div>
        <Row gutter={80}>
          <Col span={12}>
            <Row gutter={80}>
              <Col span={24}>
                <span className="form-input-label" >
                  <FormattedMessage {...basicMessages.applicant} />
                </span>
              </Col>
              <Col span={24}>
                <Input
                  disabled
                  placeholder="applicant"
                  className="form-input"
                  value={data.Applicant}
                  onChange={e => {data.Applicant = e.target.value}}
                />
              </Col>
            </Row>
          </Col>
          <Col span={12}>
            <Row gutter={80}>
              <Col span={24}>
                <span className="form-input-label" >
                  <FormattedMessage {...basicMessages.department} />
                </span>
              </Col>
              <Col span={24}>
                <Input
                  placeholder="Department"
                  disabled
                  className="form-input"
                  value={data.DepartmentName}
                />
              </Col>
            </Row>
          </Col>
        </Row>
        <Row gutter={80}>
          <Col span={12}>
            <Row gutter={80}>
              <Col span={24}>
                <span className="form-input-label" >
                  <FormattedMessage {...basicMessages.date} />
                </span>
              </Col>
              <Col span={24}>
                <DatePicker
                  placeholder="Application Date"
                  disabled
                  className="form-input"
                  value={moment(data.ApplicationDate)}
                />
              </Col>
            </Row>
          </Col>
          <Col span={12}>
            <Row gutter={80}>
              <Col span={24}>
                <span className="form-input-label" >
                  <FormattedMessage {...basicMessages.folio} />
                </span>
              </Col>
              <Col span={24}>
                <Input
                  placeholder="Folio"
                  disabled
                  className="form-input"
                  value={data.Folio}
                />
              </Col>
            </Row>
          </Col>
        </Row>
      </div>
        );
  }
};

export default BasicInfo;
