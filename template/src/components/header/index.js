import React, { Component } from 'react';
import { FormattedMessage } from 'react-intl';
import { Row, Col, Select } from 'antd';
import { Cookies }  from 'brc-util';
import { lanMessages } from '../../locales';

const Option = Select.Option;

class Header extends Component {
  constructor() {
    super();
    this.handleLanChange = this.handleLanChange.bind(this);
  }

  handleLanChange(value) {
    window.changeLanguage(value);
  }
  render() {
    const lanCookie = Cookies.get('lan');
    const defaulLan = lanCookie ? lanCookie : 'zh-CN';
    return (
      <div className="ant-form-header">
        <Row gutter={16}>
          <Col span={4}>
            <img src={this.props.logo} className="bedrock-logo" alt="logo" />
          </Col>
          <Col span={16} className="ant-form-title">
            <span>{this.props.title}</span>
          </Col>
          <Col span={4} style={{textAlign: 'right'}}>
            <Select size="small" defaultValue={defaulLan} onSelect={this.handleLanChange}>
              <Option value="zh-CN"><FormattedMessage {...lanMessages.zh} /></Option>
              <Option value="en-US"><FormattedMessage {...lanMessages.en} /></Option>
            </Select>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <div
              style={{
                height: 1,
                width: '100%',
                backgroundColor: '#d9d9d9',
              }}
            >
            </div>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Header;
