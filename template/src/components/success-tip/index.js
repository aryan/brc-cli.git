import React, { Component } from 'react';
import { Alert } from 'antd';
import './index.css';

class SuccessTip extends Component {
  render() {
    return (
      <div className="tipContainer">
        <div className="tip">
          <Alert
            message="Message"
            description="Successful operation!"
            type="success"
            showIcon
            closable
            onClose={() => {
              window.opener = null;
              window.open('', '_self');
              window.close();
            }}
          />
        </div>
      </div>
    );
  }
}

export default SuccessTip;
