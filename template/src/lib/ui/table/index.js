import React, { Component } from 'react';
import { observer } from 'mobx-react';

const Table = observer(class Table extends Component {
  render() {
    return (
      <div className="brc-table-container">
        <div className="brc-table-content">
          <div style={{ flex: 1 }}>
            <table className="brc-table">
              <thead>
                {this.props.renderHeader()}
              </thead>
              <tbody>
                {this.props.renderRow()}
              </tbody>
            </table>
          </div>
        </div>
        {
          this.props.renderFooter ?
          <div className="brc-table-footer">
            {this.props.renderFooter()}
          </div> :
          null
        }
      </div>
    );
  }
});

Table.defaultProps = {
  dataSource: [],
  footershow: 'block',
}

export default Table;
