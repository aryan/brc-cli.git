import React, { Component } from 'react';
import StaticSelect from '../static-select';
import RemoteSelect from '../remote-select';

class Select extends Component {
  render() {
    const { remote, ...rest } = this.props;
    return remote ? <RemoteSelect {...rest} /> : <StaticSelect {...rest} />
  }
}

Select.defaultProps = {
  remote: false,
}

Select.Option = StaticSelect.Option;

export default Select;