import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { Select as AntSelect, Tooltip } from 'antd';

@observer
class StaticSelect extends Component {
  constructor() {
    super();
    this.handleBlur = this.handleBlur.bind(this);
  }
  handleBlur() {
    const { validator } = this.props;
    if(validator) validator.touched=true;
  }
  render() {
    const { validator, ...rest } = this.props;
    let hasError = false;
    if(validator) {
      if (validator.touched&&validator.error!==''&&validator.error) {
        hasError = true;
      }
    }
    return (
      <Tooltip placement="topLeft" title={hasError ? validator.error : ''}>
        <div className={hasError ? 'has-error' : ''}>
          <AntSelect {...rest} onBlur={this.handleBlur} >
            {this.props.children}
          </AntSelect>
        </div>
      </Tooltip>
    );
  }
}

StaticSelect.Option = AntSelect.Option;

export default StaticSelect;
