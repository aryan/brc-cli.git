import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { Input as AntInput, Tooltip } from 'antd';
import '../base.css';

const { TextArea } = AntInput

@observer
class Input extends Component {
  constructor() {
    super();
    this.handleBlur = this.handleBlur.bind(this);
  }
  handleBlur() {
    const { validator } = this.props;
    if(validator) validator.touched=true;
  }
  render() {
    const { validator, ...rest } = this.props;
    let hasError = false;
    if(validator) {
      if (validator.touched&&validator.error) {
        hasError = true;
      }
    }
    return (
      <Tooltip placement="topLeft" title={hasError ? validator.error : ''}>
        <div className={hasError ? 'has-error' : ''}>
          <AntInput {...rest} onBlur={this.handleBlur} />
        </div>
      </Tooltip>
    );
  }
}

Input.TextArea = TextArea;

export default Input;
