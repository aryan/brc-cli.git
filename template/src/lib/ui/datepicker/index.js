import React, { Component } from 'react';
import { DatePicker as AntDatePicker, Tooltip } from 'antd';
import { observer } from 'mobx-react';

@observer
class DatePicker extends Component {
  constructor() {
    super();
    this.handleBlur = this.handleBlur.bind(this);
  }
  handleBlur() {
    const { validator } = this.props;
    if(validator) validator.touched=true;
  }
  render() {
    const { validator, ...rest } = this.props;
    let hasError = false;
    if(validator) {
      if (validator.touched&&validator.error!==''&&validator.error) {
        hasError = true;
      }
    }
    return (
      <Tooltip placement="topLeft" title={hasError ? validator.error : ''}>
        <div className={hasError ? 'has-error' : ''}>
          <AntDatePicker {...rest} onOpenChange={this.handleBlur} />
        </div>
      </Tooltip>
    );
  }
}

export default DatePicker;
