import React from 'react';
import ReactDOM from 'react-dom';
import { LocaleProvider } from 'antd';
import { addLocaleData, IntlProvider } from 'react-intl';
import { Cookies } from 'brc-util';

const changeLanguage = function(lan) {
  Cookies.set('lan', lan);
  window.location.reload();
}
const initLanguage = function() {
  //const lan = this.state.language;
  let lan = Cookies.get('lan');
  if(!lan) lan = 'zh-CN';
  const appLocale = require(`../../locales/${lan}`).default;
  addLocaleData(appLocale.data);
  return appLocale;
}
const renderView = function(app) {
  const appLocale = initLanguage();
  ReactDOM.render(
    <LocaleProvider locale={appLocale.antd}>
      <IntlProvider locale={appLocale.locale} messages={appLocale.messages}>
        {app}
      </IntlProvider>
    </LocaleProvider>,
    document.getElementById('root')
  );
}
window.changeLanguage = changeLanguage;
export {
  changeLanguage,
  initLanguage,
  renderView
}