import { observable } from 'mobx';

class BasicInfoModel {
  @observable ProcessGuid = '';
  @observable ActivityGuid = '';
  @observable Applicant = '';
  @observable ApplicantAd = '';
  @observable DepartmentName = '';
  @observable DepartmentCode = '';
  @observable NextApprover = '';
  @observable ApplicationDate = '';
  @observable ProcessStatus = '';
  @observable TotalAmount = 0.00;
  @observable Folio = '';
  @observable Year = '';
  @observable Month = '';
}

export default new BasicInfoModel();
