import { observable, action } from 'mobx';
import commentModel from './comment-model';

class CommentList {

  @observable list = [];

  @action
  load(data) {
    data.forEach(item => {
      const model = new commentModel();
      const dataModel = Object.assign(model, item);
      this.list.push(dataModel);
    });
  }
}

export default new CommentList();
