import { observable, computed, action } from 'mobx';
import ListModel from './list-model';

class ListInfo {

  @observable list = [];

  @observable
  modalVisibled = false;

  add() {
    this.list.push(new ListModel());
  }

  remove(index) {
    this.list.splice(index, 1);
  }

  @action
  load(data) {
    data.forEach(item => {
      const model = new ListModel();
      Object.assign(model, item);
      this.list.push(model);
    });
  }

  @action
  checkValid() {
    let result = true;
    this.list.forEach(item => {
      if (!item.isValid) {
        item.markTouched();
        result = false;
      }
    });
    return result;
  }

  @action
  saveCellData(rowIndex, data) {
    Object.assign(this.list[rowIndex], data);
    this.modalVisibled = false;
  }

  @computed get reportTotalAmount() {
    let totalAmount = 0.00;
    this.list.forEach(item => {
      if (item.Amount !== '') {
        totalAmount += parseFloat(item.Amount);
      }
    });
    return totalAmount.toFixed(2);
  }

  @computed get reportPostDataList() {
    const dataList = this.list.map(item => {
      return item.reportPostModel;
    });
    return dataList;
  }
}

export default new ListInfo();
