import { observable } from 'mobx';
import { Http } from 'brc-util';
import api from '../api';

class EmployeeStore {
  constructor() {
    this.loadData();
  }

  @observable CostCenterCode = '';
  @observable CostCenterName = '';
  loadData() {
    Http.get(`${api}/BRC.Reimbursement.API/process/getcostcenter1`)
      .then(json => {
        Object.assign(this, {
          CostCenterCode: json.Code,
          CostCenterName: json.CName,
        });
      })
  }
}

export default new EmployeeStore();
