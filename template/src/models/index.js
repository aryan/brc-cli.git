import { observable } from 'mobx';

class Store {
  @observable submited = false;
  @observable commentboxVisabled = false;
  @observable commentText = '';
}

export default new Store();