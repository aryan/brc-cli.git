import { observable, action, computed } from 'mobx';
import { Validator, ValidateModel } from 'brc-util';

class LeaveInfo extends ValidateModel {

  @observable
  @Validator.required()
  EventDate = new Date();

  @observable
  CostCenterName = '';

  @observable
  CostCenterCode = '';

  @observable
  ExpenseCategoryName = '';

  @observable
  ExpenseCategoryCode = '';

  @observable
  @Validator.currency()
  Amount = '';

  @observable
  Remark = '';

  /**
   * 成本中心改变后的事件
   * 
   * @param {any} value 
   * @memberof LeaveInfo
   */
  @action
  onCostCenterCodeChange(value, text) {
    this.CostCenterCode = value;
    this.CostCenterName = text;
    this.ExpenseCategoryName = '';
    this.ExpenseCategoryCode = '';
  }

  /**
   * 费用类型改变后的处理事件
   * 
   * @param {any} value 
   * @memberof LeaveInfo
   */
  @action
  onExpenseCategoryCodeChange(value, text) {
    this.ExpenseCategoryCode = value;
    this.ExpenseCategoryName = text;
    this.AccountsName = '';
    this.AccountsCode = '';
  }

  @computed get reportPostModel() {
    return {
      EventDate: this.EventDate,
      CostCenterCode: this.CostCenterCode,
      AccountsCode: this.AccountsCode,
      ExpenseCategoryCode: this.ExpenseCategoryCode,
      RelevantCategoryCode: this.RelevantCategoryCode,
      CustomerCode: this.CustomerCode,
      ProjectCode: this.ProjectCode,
      CaseCode: this.CaseCode,
      Amount: this.Amount,
      Remark: this.Remark,
    }
  }
}
export default LeaveInfo;