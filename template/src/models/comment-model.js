import { observable } from 'mobx';

class CommentModel {
  @observable Action = '';
  @observable ActivityGuid = '';
  @observable ActivityName = '';
  @observable Content = '';
  @observable CreateDate = '';
  @observable Operator = '';
  @observable OperatorAd = '';
  @observable ProcessGuid = '';
}

export default CommentModel;